#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data

echo "==> Starting Sterling-PDF"
exec java -jar /app/code/S-PDF.jar
