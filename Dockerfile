FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

ARG VERSION=0.10.2

RUN sudo apt-get update && sudo apt-get install -y openjdk-17-jdk libreoffice-writer libreoffice-calc libreoffice-impress unpaper ocrmypdf && pip3 install uno opencv-python-headless unoconv pngquant

RUN mkdir -p /app/code
WORKDIR /app/code
ADD https://github.com/Frooodle/Stirling-PDF/releases/download/v${VERSION}/S-PDF.jar /app/code

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
